<?php include("start.php"); custom_start();
	//If not verified, do not continue, redirect back to login.php
        if($_SESSION['loginVerified'] != "success") {
	        header('Location: login.php');
		exit;
	}
        $alert_emailChanged = "";
        $alert_emailValid = "";
        $alert_dupUsername = "";
        if(isset($_SESSION['change_success'])) {
	  $message = "Email verified";
	  $alert_emailValid = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
	  unset($_SESSION['change_success']);
	}
        if(isset($_POST['user']) && isset($_POST['email'])) {
	        $user = preg_replace("/[^a-zA-Z0-9_]/i","",$_POST['user']);
		$user = strtolower($user);
		$email = $_POST['email'];
		$old_user = $_SESSION['user'];
		$data = json_decode(file_get_contents("users/$old_user/details.json"),true);
		if($email != $data['email']) {
		  //verify new email address
		  $code = md5(uniqid(rand()));
		  $data["confirm"] = $code;
		  $data["validated"] = False;
		  $data["prev_email"] = $data["email"];
		  $data["email"] = $email;
		  unlink("users/$old_user/details.json");
		  file_put_contents("users/$old_user/details.json", json_encode($data));
		  $link = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/verify.php?confirm=$code&e_changed=True";
		  $link = preg_replace("/^cgi/i","http://www",$link);
		  $from = 'from: noreplay@budgetapprecovery.com';
		  $subject = "New email confirmation";
		  $msg = "Hello $user, \r\n";
		  $msg .= "Click on the link below to validate your new email \r\n";
		  $msg .= $link;
		  $msg .= "\r\n";
		  mail($email,$subject,$msg,$from);
		  $message = 'Email changed. Please check your email to verify this account';
		  $alert_emailChanged = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
		}
		if($old_user != $user && !is_dir("users/$user")) {
		  //copy all files in users/$old_user to users/$user
		  //and modify all mentions of $old_user to $user in each file
		  //unlink users/$old_user
		  mkdir("users/$user");
		  $_SESSION['user'] = $user;
		  $entries = scandir("users/$old_user");
		  //user files
		  foreach($entries as $entry) {
		    if($entry != "." && $entry != "..") {
		      $filestring = file_get_contents("users/$old_user/$entry");
		      //replace all mentions of $old_user with $user (only in details.json)
		      if($entry == "users/$old_user/details.json") {
			$pattern = "/($old_user)/i";
			$filestring = preg_replace($pattern,$user,$filestring,1);
		      }
		      file_put_contents("users/$user/$entry",$filestring);
		      unlink("users/$old_user/$entry");
		    } 
		  }
		  //pie image
		  rename("cache/$old_user-draw3DPie.png","cache/$user-draw3DPie.png");
		  rmdir("users/$old_user");
		} elseif(is_dir("users/$user") && $old_user != $user) {
		  //username already exists
		  $message = "Username '$user' already exists";
		  $user = $old_user;
		  $alert_dupUsername = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
		}
	} else {
	        $user = $_SESSION['user'];

		if(is_dir("users/$user")) {
		        $data = json_decode(file_get_contents("users/$user/details.json"),true);
			$email = $data['email'];
		}
	}
?>

<html lang="en">
<head>
	<?php include 'headerInfo.php' ?>
</head>
<script>
	function deleteConfirm() {
	  var r = confirm("Warning: You are about to delete your account, do you wish to contine?");
	  return r;
	}
</script>
<body>
<?php include 'navbar.php' ?>
	<div class="container">
		<div class='jumbotron'>

	  <div class="block-center container-fluid">
	  
	  <?php echo $alert_emailChanged;echo $alert_dupUsername;echo $alert_emailValid; ?>
	    <div class="row-fluid panel panel-default">
	      <div class="panel-heading">
	        <h4 class="panel-title"><strong>Personal Information</strong></h4>
	      </div>
	      <div class="list-group">
	        <div class="list-group-item">
	          <div class="row">
	            <div class="col-xs-12 col-sm-4 col-md-4 ">
	              <b>Account Name</b>	  
	            </div>
	            <div class="col-xs-12 col-sm-8 col-md-8">
	              <b><?php echo ucfirst($user); ?></b>
	            </div>
	          </div>
	        </div>
	        <div class="list-group-item">
	          <div class="row">
	            <div class="col-xs-12 col-sm-4 col-md-4">
	              <b>Email</b>	  
	            </div>
	            <div class="col-xs-12 col-sm-8 col-md-8">
	              <b><?php echo $email; ?></b>
	            </div>
	          </div>
	        </div>
	        <div class="list-group-item">
	          <div class="row">
	            <div class="col-xs-12 col-sm-4 col-md-4">
	              <b>Password</b>
	            </div>
	            <div class="col-xs-12 col-sm-8 col-md-8">
	              <a href="changePassword.php">Change your password</a>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	    <div class="btn-toolbar">
	      <a class="btn btn-primary" href="update.php">Edit</a>
	      <a class="btn btn-danger" href="delete.php" onclick="return deleteConfirm()">Delete account</a>
	    </div>
	  </div>

		</div>
	</div>
</body>
</html>
