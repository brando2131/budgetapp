<?php include("start.php"); custom_start();
        $requestor = "";
        $found = False;
        if(isset($_GET['confirm'])) {
	        $code = $_GET['confirm'];
		//Find the account with the matching confirm code
		$path = "users/";
		$users = glob($path . '/*', GLOB_ONLYDIR);
		foreach($users as $user) {
		        $filename = "$user/details.json";
			$data = json_decode(file_get_contents($filename),true);
			// Remove the 'confirm' field once the account is validated
			if(isset($data["confirm"]) && $code == $data["confirm"]) {
			        $data['validated'] = True;
				unset($data["confirm"]);
				unset($data["prev_email"]);
				unlink($filename);
				file_put_contents($filename,json_encode($data));
				$found = True;
				$requestor = $user;
				break;
			}
		}
		$_SESSION['acc_validation'] = $found;
		$_SESSION['acc_validation_msg'] = $found ? "Your account has been activated" : "Wrong confirmation code";
	}
        if(isset($_GET['e_changed']) && $found && $_GET['e_changed'] == "True") {
	        $_SESSION["loginVerified"] = "success";
		$_SESSION["user"] = preg_replace("/^users\/\//i","",$requestor);
		$_SESSION["change_success"] = True;		
		header('Location: account.php');
	} else {
	        header('Location: login.php');  
	}
?>