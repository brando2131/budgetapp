<?php include("start.php"); custom_start();

        if($_SESSION['loginVerified'] != "success") {
	        header('Location: login.php');
		exit;
	}
        $user = $_SESSION['user'];
        $message = "";
        $success = False;
        if(is_dir("users/$user") && isset($_POST['oldpass']) && isset($_POST['pass1']) && isset($_POST['pass2'])) {
	        $data = json_decode(file_get_contents("users/$user/details.json"),true);
		$oldpass = $_POST['oldpass'];
		$pass1 = $_POST['pass1'];
		$pass2 = $_POST['pass2'];
		if(hash("sha512",$oldpass.$data['salt']) == $data['hash']) {
		        if(strlen($pass1) >= 6) {
			        if($pass1 == $pass2) {
				        $salt = substr(str_replace('+','.',base64_encode(md5(mt_rand(), true))),0,16);
					$hash = hash("sha512",$pass1.$salt); 
					$data['hash'] = $hash;
					$data['salt'] = $salt;
					unlink("users/$user/details.json");
					file_put_contents("users/$user/details.json",json_encode($data));
					$success = True;
					$message = "Password has successfully changed. ";
				} else {
				        $message = "Passwords do not match!";
				}
			} else {
			        $message = "Passwords must be at least 6 characters";
			}
		} else {
		        $message = "Incorrect password!";
		}
		if($success) {
		        $alert = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'<a href="account.php">Back to accounts page</a></strong></div>';	  
		} else {
		        $alert = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';	  
		}
	}

?>

<html lang="en">
<head>
   <?php include 'headerInfo.php' ?>
</head>
<body>
   <?php include 'navbar.php' ?>
   <div class="container-fluid">
      <div class="jumbotron">

	  <div class="block-center container-fluid">
		<?php echo $alert; ?>
	  <!--a href="account.php"><?php var_dump($_SESSION);var_dump($data); ?></a-->
	  <form action="changePassword.php" method="POST">
	     <h3 class="form-signin-heading">Change Password</h3>
	     <div class="form-group">
		<input type="password" class="form-control" style="margin-bottom:-1px; border-top-left-radius: 0;border-top-right-radius: 0; width: 300px;" name="oldpass" id="oldpass" placeholder="Old Password" required autofocus>					    
		
		<input type="password" class="form-control" style="margin-bottom:-1px; border-top-left-radius: 0;border-top-right-radius: 0; width: 300px;" name="pass1" id="pass1" placeholder="New Password" required>
                
		<input type="password" class="form-control" style="margin-bottom:-1px; border-top-left-radius: 0;border-top-right-radius: 0; width: 300px;" name="pass2" id="pass2" placeholder="Retype Password" required>
             </div>
	     <div class="form-group">		    
		<button type="submit" class="btn btn-primary">Change</button>
		<a href="account.php" class="btn btn-default">Cancel</a>
	     </div>
	     <div class="form-group" style="width: 300px;">
	     </div>
	  </form>

      </div>
      </div>
   </div>
</body>
</html>
