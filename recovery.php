<?php include("start.php"); custom_start();
        //If the user has already entered their details send email
        $alert = '';
        $set = False;
        $message = "";
        if(isset($_POST['user']) && isset($_POST['email'])) {
	    $user = $_POST['user'];
	    $email = $_POST['email'];
	    if(is_dir("users/$user")) {
	        $data = json_decode(file_get_contents("users/$user/details.json"),true);
		if($data["email"] == $email) {
		    $set = True;
		    $from = "From: <noreply@budgetapprecovery.com>";
		    $salt = substr(str_replace('+','.',base64_encode(md5(mt_rand(), true))),0,16);
		    $password = substr(md5($salt.$user.$email),0,8);
		    $msg = "Hello ".$user.",\nYour new password is: '".$password."'";
		    foreach($data as $key => $val) {
		      if($key == "salt" || $key == "hash") {
			$USER_INFO["salt"] = $salt;
			$USER_INFO["hash"] = hash("sha512",$password.$salt);
		      } else {
			$USER_INFO[$key] = $val;
		      }
		    }
		    unlink("users/$user/details.json");
		    file_put_contents("users/$user/details.json",json_encode($USER_INFO));
		    mail($email,"Password recovery",$msg,$from);
		    $status = 'success';
		} else {
		    $message .="Incorrect Username/Email";
		    $alert = '<div class="alert alert-warning fade in" style="width: 250px;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
		}
	    } else {
	        $message .= "Incorrect Username/Email";
		$alert = '<div class="alert alert-warning fade in" style="width: 250px;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';

	    }
	} 
        
?>
<html lang="en">
<head>
	<?php include 'headerInfo.php' ?>
</head>
<body>
        <div class="container">
                <!-- This form POSTS to recovery.php with user and email fields -->
	        <h3>Account Recovery</h3>
	        <h4>Please enter your user ID and email or <a href="login.php">login.</a></h4>
     	        
	                <form class="form-horizontal" method="POST">
	                <?php echo $alert; ?>
	                <div class="form-group">
	                        <label for="user" class="col-sm-2 control-label">Username</label>
	                        <div class="col-sm-10">
	                                <input type="username" class="form-control" name="user" id="user" placeholder="Username" style="width: 300px;" required autofocus>
	                        </div>
	                </div>
	                <div class="form-group">
	                        <label for="email" class="col-sm-2 control-label">Email</label>
	                        <div class="col-sm-10">
	                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" style="width: 300px;" required>
	                        </div>
	                </div>
	                <div class="form-group">
	                        <div class="col-sm-offset-2 col-sm-10">
	                                <button type="submit" class="btn btn-default btn-primary">Send Email</button>
	                        </div>
	                </div>
	                <?php if($set) { ?>
                        <h4>Please check your email for your new password.</h4><br>
			<h4><a href="login.php">Login</a></h4>
			<?php } ?>
	        </form>
	</div>
</body>
</html>