<?php include("start.php"); custom_start();
	//If the user is already logged in and verified, redirect to index.html
	if($_SESSION["loginVerified"] === "success"){
		header('Location: index.php');
	}
	
	$message = '';
	//Sent out warning message that username/password is incorrect
	if($_SESSION["loginVerified"] === "failed"){
	        if(isset($_SESSION["verify_email_msg"])) {
	                $message = $_SESSION["verify_email_msg"];
			unset($_SESSION["verify_email_msg"]);
		} else {
	                $message = "Incorrect Username/Password";
		}
		$alert = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$message.'</strong></div>';
		$_SESSION["loginVerified"] = "unknown";
	}
        
        $confirm = '';
        if(isset($_SESSION["acc_validation"])) {
	        $msg = $_SESSION["acc_validation_msg"];
		if($_SESSION["acc_validation"]) {
		        $confirm = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$msg.'</strong></div>';
		} else {
		        $confirm = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$msg.'</strong></div>';
		}
		unset($_SESSION["acc_validation_msg"]);
		unset($_SESSION["acc_validation"]);
	} 
?>
<html lang="en">
<head>
	<?php include 'headerInfo.php' ?>
</head>
<body>
	<div class="container">
	
		<!-- This form POSTs to index.php with the user and pass fields -->

		<form class="form-signin" action="index.php" method="POST">
			<center><h1><img src="logo.png" alt="Logo" height="60" width="60"> myBudget</h1></center><br>
			<h3 class="form-signin-heading">Please sign in</h3>
			<h4>Or click here to <a href="register.php">register</a></h4>
	                <?php echo $confirm; ?>
			<?php echo $alert; ?>
			<input type="username" name="user" id="user" class="form-control" placeholder="Username" required autofocus>
			<input type="password" name="pass" id="pass" class="form-control" placeholder="Password" required>
	                <h5><a href="recovery.php">Forgot password?</a></h5>
			<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
		</form>
	</div>
	
</body>

</html>