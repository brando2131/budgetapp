<?php include("start.php"); custom_start();
	//If not verified, do not continue, redirect back to login.php
	if($_SESSION["loginVerified"] != "success"){
		header('Location: login.php');
		exit;
	}
	
	if (!isset($_GET['sort'])) {
		header('Location: viewExpense.php?sort=Date');
		exit;
	}
	
	if (isset($_GET['delete']) && file_exists("users/".$_SESSION['user']."/expenses.json")) {
		$EXP_DEL = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
		$counter = 0;
		foreach ($EXP_DEL as $del) {
			if ($del['id'] == $_GET['delete']) {
				unset($EXP_DEL[$counter]);
				$EXP_DEL = array_values($EXP_DEL);
			}
			$counter++;
		}
		file_put_contents("users/".$_SESSION['user']."/expenses.json", json_encode($EXP_DEL));
	}
?>

<html lang="en">
<head>
	<?php include 'headerInfo.php' ?>
</head>
<body>

<?php include 'navbar.php' ?>

	<div class="container">
		<div class='jumbotron'>
			<legend>Your expenses</legend>
			<p>
			<form class="form-inline" method="GET">
				<div class="form-group">
					<label for="sort">Sort by:</label>
					<br>
					
					<?php
						echo "<a class=\"btn btn-";
						if ($_GET['sort'] == "Date") {
							echo "primary";
						} else {
							echo "default";
						}
						echo"\" href=\"viewExpense.php?sort=Date\" type=\"submit\" role=\"button\" name=\"sort\">Date</a>";
					
						echo "<a class=\"btn btn-";
						if ($_GET['sort'] == "Amount") {
							echo "primary";
						} else {
							echo "default";
						}
						echo"\" href=\"viewExpense.php?sort=Amount\" type=\"submit\" role=\"button\" name=\"sort\">Amount</a>";
					
						echo "<a class=\"btn btn-";
						if ($_GET['sort'] == "Category") {
							echo "primary";
						} else {
							echo "default";
						}
						echo"\" href=\"viewExpense.php?sort=Category\" type=\"submit\" role=\"button\" name=\"sort\">Category</a>";
					
					?>
					
				</div>
			</form>
			<table class="table table-striped" style="width: 100%;">
				<thead>
					<tr>
						<th>Date</th>
						<th>Amount</th>
						<th>Category</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(file_exists("users/".$_SESSION['user']."/expenses.json")){
						$U_EXPENSE_DECODE = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
						
						echo "\n";
						//Print each entity for table
						$i = 0;
						
						if ($_GET['sort'] == "Date") {
							foreach ($U_EXPENSE_DECODE as $d) {
								$days[] = $d['day'];
							}
							array_multisort($days, SORT_NUMERIC, SORT_DESC, $U_EXPENSE_DECODE);
							foreach ($U_EXPENSE_DECODE as $m) {
								$months[] = $m['month'];
							}
							array_multisort($months, SORT_NUMERIC, SORT_DESC, $U_EXPENSE_DECODE, SORT_DESC);
							
						} else if ($_GET['sort'] == "Amount") {
							foreach ($U_EXPENSE_DECODE as $a) {
								$amounts[] = $a['amount'];
							}
							array_multisort($amounts, SORT_NUMERIC, SORT_DESC, $U_EXPENSE_DECODE);
							
						} else if ($_GET['sort'] == "Category") {
							foreach ($U_EXPENSE_DECODE as $c) {
								$cats[] = $c['type'];
							}
							array_multisort($cats, SORT_STRING, SORT_ASC, $U_EXPENSE_DECODE);
							
						}
						
						foreach($U_EXPENSE_DECODE as $v){
							echo "<tr>\n";
					
							if ($v['day'] < 10 && $v['month'] < 10) {
								echo "<td>".'0'.$v['day'].'/0'.$v['month'].'/'.$v['year']."</td>\n";
							} else if ($v['day'] < 10 && $v['month'] > 9) {
								echo "<td>".'0'.$v['day'].'/'.$v['month'].'/'.$v['year']."</td>\n";
							} else if ($v['day'] > 9 && $v['month'] < 10) {
								echo "<td>".$v['day'].'/0'.$v['month'].'/'.$v['year']."</td>\n";
							} else {
								echo "<td>".$v['day'].'/'.$v['month'].'/'.$v['year']."</td>\n";
							}
							echo "<td>".'$'.money_format("%.2n",$v['amount'])."</td>\n";
							echo "<td>".$v['type']."</td>\n";
							echo "<td>".$v['description']."</td>\n";
							echo "<td>\n<form action=\"viewExpense.php\" method=\"get\">\n<input type=\"hidden\" value=\"".$_GET['sort']."\" name=\"sort\">\n";
							echo "<input type=\"hidden\" value=\"".$v['id']."\" name=\"delete\">\n";
							echo "<input type=\"submit\" class=\"btn btn-danger btn-xs\" value=\"X\"></form></td>\n";
							echo "</tr>\n";
							$i++;
						}
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
