<?php include("start.php"); custom_start();
        function removeAll($dir) {
	  if(is_dir($dir)) {
	    $entries = scandir($dir);
	    foreach($entries as $entry) {
	      if($entry != "." && $entry != "..") {
		if(filetype($dir."/".$entry) == "dir") {
		  removeAll($dir."/".$entry);
		} else {
		  unlink($dir."/".$entry);
		}
	      }
	    }
	    reset($entries);
	    rmdir($dir);
	  }
	}
        if($_SESSION['loginVerified'] != "success") {
	        header('Location: login.php');
		exit;
	}

        $user = $_SESSION['user'];
        if(is_dir("users/$user")) {
	        removeAll("users/$user");
	}
        unlink("cache/$user-draw3DPie.png");
        session_destroy();
        header('Location: login.php');
        exit;
?>
