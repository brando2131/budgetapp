<?php include("start.php"); custom_start();
	//If form has been posted to itself, attempt to create the account
	//Otherwise this is ignored
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$user = strtolower($_POST['user']);
		$pass1 = $_POST['pass1'];
		$pass2 = $_POST['pass2'];
		$email = $_POST['email'];

		//Alphanumeric usernames only
		$status = 'error';
		if(ctype_alnum($user)){
			//Check if user already exists
			if(is_dir("users/$user")){
				$message = 'Username already exists';
			}
			else{
				//Check if passwords are the same
				if($pass1 == $pass2){
					if(strlen($pass1) >= 6){
						//Generate md5 salt from user for password hash
						$salt = substr(str_replace('+','.',base64_encode(md5(mt_rand(), true))),0,16);
						$confirm_code = md5(uniqid(rand()));
						$link = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/verify.php?confirm=".$confirm_code;
						$link = preg_replace('/^cgi/i','http://www',$link);
						
						$subject = 'Your confirmation link';

						$msg = "Your confirmation link \r\n";
						$msg .= "Click on this link to activate your account \r\n";
						$msg .= $link;

						//$msg .= "http://www.cse.unsw.edu.au/~z3417908/budgetApp/budgetapp/verify.php?confirm=$confirm_code";
						$msg .= "\nOnce you click the link you will be able to login immediately.";
						$from = "From: <noreply@budgetappverification.com>";
						
						//Create the user folder
						mkdir("users/$user");
						$USER_INFO["name"] = $user;
						$USER_INFO["email"] = $email;
						$USER_INFO["validated"] = False;
						$USER_INFO["salt"] = $salt;
						$USER_INFO["hash"] = hash("sha512", $pass1.$salt);
						$USER_INFO["confirm"] = $confirm_code;
						
						file_put_contents("users/$user/details.json", json_encode($USER_INFO));						
						//success
						$message = "Account has been created. Please check your email to verify this account. ";
						
						$status = 'success';
						mail($email,$subject,$msg,$from);
					}else{
						$message = 'Password must be at least 6 characters';
					}
				}else{
					$message = 'Passwords are not the same';
				}
			}
		}else{
			$message = 'Usernames can only contain the following: a-z, A-Z, 0-9. No spaces or special characters allowed.';
		}
	}else{
		$message = '';
	}
	
	if($message == ''){ //No alert box
		$alert = '';
	}else if($status == 'success'){ //Green alert box
		$alert = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong> <a href="login.php">Sign in here</a></div>';
	}else{ //Red alert box
		$alert = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
	}
?>
<html lang="en">
<head>
	<?php include 'headerInfo.php'; ?>
</head>
<body>
	<div class="container">
	
		<!-- This form POSTs to itself, register.php with the user, pass1, pass2 fields into the above php script -->
		
		<form class="form-signin" action="register.php" method="POST">
			<center><h1><img src="logo.png" alt="Logo" height="60" width="60"> myBudget</h1></center><br>
			<h3>User account registration</h3>
			<h4>Or click here to <a href="login.php">login</a></h4>
			<?php //Display the alert box
				echo $alert; 
			?>
			<input type="username" name="user" id="user" class="form-control" placeholder="Username" required autofocus>
			<input type="text" style="margin-bottom: -1px;border-top-left-radius: 0;border-top-right-radius: 0;" name="email" class="form-control" placeholder="Email" required>
			<input type="password" style="margin-bottom: -1px;border-top-left-radius: 0;border-top-right-radius: 0;" name="pass1" id="pass1" class="form-control" placeholder="Password" required>
			<input type="password" style="margin-bottom: 10px;border-top-left-radius: 0;border-top-right-radius: 0;" name="pass2" id="pass2" class="form-control" placeholder="Retype Password" required>
			
			<button type="submit" class="btn btn-lg btn-primary btn-block">Create Account</button>
		</form>
	</div>
</body>

</html>
