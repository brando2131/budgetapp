<?php include("start.php"); custom_start();

        if($_SESSION['loginVerified'] != "success") {
	        header('Location: login.php');
		exit;
	}
        $user = $_SESSION['user'];
        
	  $budgetAmount = 0;
	  $budgetFreq = 0;
	  $incomeAmount = 0;
	  $incomeFreq = 0;
        if(is_dir("users/$user")) {
	  $data = json_decode(file_get_contents("users/$user/budget.json"),true);
	  $budgetAmount = $data['budgetAmount'];
	  $budgetFreq = $data['budgetFreq'];
	  $incomeAmount = $data['incomeAmount'];
	  $incomeFreq = $data['incomeFreq'];
	}

?>

<html lang="en">
<head>
<?php include 'headerInfo.php' ?>
</head>
<body>
<?php include 'navbar.php' ?>
   <div class="container">
     <div class="jumbotron">

       <div class="block-center">
         
           <legend class="block-center-heading">Update your details</legend><br>
			<form class="form-inline" action="index.php" method="POST">
				<label class="control-label col-sm-3">Enter your budget</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input type="text" name="budgetAmount" id="budgetAmount" class="form-control" id="amount" value=<?php echo "\"".$budgetAmount."\""; ?>>
					</div>
					<select type="text" name="budgetFreq" id="budgetFreq" class="form-control" id="amount">
						<option value="weekly" <?php if($budgetFreq == "weekly"){echo "selected=\"selected\"";} ?>>Weekly</option>
						<option value="fortnightly" <?php if($budgetFreq == "fortnightly"){echo "selected=\"selected\"";} ?>>Fortnightly</option>
						<option value="monthly" <?php if($budgetFreq == "monthly"){echo "selected=\"selected\"";} ?>>Monthly</option>
					</select>
				</div>
				<br><br>
				<label class="control-label col-sm-3">Enter your income</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input type="text" name="incomeAmount" id="incomeAmount" class="form-control" id="amount" value=<?php echo "\"".$incomeAmount."\""; ?>>
					</div>
					<select type="text" name="incomeFreq" id="incomeFreq" class="form-control" id="amount">
						<option value="weekly" <?php if($incomeFreq == "weekly"){echo "selected=\"selected\"";} ?>>Weekly</option>
						<option value="fortnightly" <?php if($incomeFreq == "fortnightly"){echo "selected=\"selected\"";} ?>>Fortnightly</option>
						<option value="monthly" <?php if($incomeFreq == "monthly"){echo "selected=\"selected\"";} ?>>Monthly</option>
						<option value="annually" <?php if($incomeFreq == "annually"){echo "selected=\"selected\"";} ?>>Annually</option>
					</select>
				</div>
				<br>
				<br>
			  	<label class="col-sm-3 control-label"></label>
				<button type="submit" value="submit" name="budgetSubmit" class="btn btn-primary">Submit</button>
				<button href="index.php" class="btn btn-default">Cancel</button>
			</form>
			
       </div>

     </div>
   </div>
</body>
</html>
