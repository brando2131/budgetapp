<?php include("start.php"); custom_start();
	//Main index.php for webapp
	
	//Verify user
	//Check if username and password have been POSTED to this php file (from login.php)
	if(isset($_POST["user"]) && isset($_POST["pass"])){
		$_SESSION["loginVerified"] = "failed";
		//Put user name in session variables and convert to lower case
		$_SESSION["user"] = strtolower($_POST["user"]);
		//Username must be alphanumic
		if(ctype_alnum($_SESSION["user"])){
			//Check if user folder exists
			if(is_dir("users/".$_SESSION['user'])){
				//Grab details.json from user folder into data to compare hash
				$data = json_decode(file_get_contents("users/".$_SESSION['user']."/details.json"), true);
				//Existing user attempted to log in without valid email
				if(isset($data['prev_email']) && $data['validated']) {
				  $data['email'] = $data['prev_email'];
				  $data['validated'] = True;
				  unset($data['prev_email']);
				  unlink("users/".$_POST["user"]."/details.json");
				  file_put_contents("users/".$_POST["user"]."/details.json", json_encode($data));				  
				}
				//Check hashed password matches hashed password in json details file
				if(hash("sha512", $_POST["pass"].$data['salt']) == $data['hash'] && $data['validated']){
					$_SESSION["loginVerified"] = "success"; //User Verified
				}
				if(!$data['validated'] && $_SESSION["loginVerified"] != "success") {
				  $_SESSION["verify_email_msg"] = "Please verify your email before logging in.";
				}
			}
		}
	}
	
	//If not verified, do not continue, redirect back to login.php
	if($_SESSION["loginVerified"] != "success"){
		header('Location: login.php');
		exit;
	}

	//Budget form values
	$message = '';
	$status = 'error';
	if(isset($_POST['budgetSubmit'])){
		if(empty($_POST['budgetAmount'])){
			$message = "Your budget cannot be empty";
		}else if(empty($_POST['incomeAmount'])){
			$message = "Your income cannot be empty";
		}else if(!is_numeric($_POST['budgetAmount']) || $_POST['budgetAmount'] <= 0){
			$message = "Your budget must be a positive amount";
		}else if(!is_numeric($_POST['incomeAmount']) || $_POST['incomeAmount'] <= 0){
			$message = "Your income must be a positive amount";
		}else{
			$message = "Your weekly budget and income has been successfully updated";
			$USER_BUDGET['budgetAmount'] = $_POST['budgetAmount'];
			$USER_BUDGET['budgetFreq'] = $_POST['budgetFreq'];
			$USER_BUDGET['incomeAmount'] = $_POST['incomeAmount'];
			$USER_BUDGET['incomeFreq'] = $_POST['incomeFreq'];
			$user = $_SESSION['user'];
			file_put_contents("users/$user/budget.json", json_encode($USER_BUDGET));
			$status = 'success';
		}
	}
	
	if($message == ''){ //No alert box
		$alert = '';
	}else if($status == 'success'){ //Green alert box
		$alert = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
	}else{ //Red alert box
		$alert = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
	}
?>
<html lang="en">
<head>
	<?php include 'headerInfo.php' ?>
</head>
<body>

<?php include 'navbar.php' ?>
	
<div class="container">
	<div class='jumbotron'>
		<h2>Hi <?php echo ucfirst($_SESSION["user"])?>,</h2>
		<br>
		
		<?php
		
		echo $alert;
		
		if(!file_exists("users/".$_SESSION['user']."/budget.json")) {
		?>
		<legend>Getting started</legend>
		<div style="padding: 20px">
			<form class="form-inline" action="index.php" method="POST">
				<label class="control-label col-sm-3">Enter your budget</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input type="text" name="budgetAmount" id="budgetAmount" class="form-control" id="amount" placeholder="0">
					</div>
					<select type="text" name="budgetFreq" id="budgetFreq" class="form-control" id="amount" placeholder="0">
						<option value="weekly">Weekly</option>
						<option value="fortnightly" selected="selected">Fortnightly</option>
						<option value="monthly">Monthly</option>
					</select>
				</div>
				<br><br>
				<label class="control-label col-sm-3">Enter your income</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input type="text" name="incomeAmount" id="incomeAmount" class="form-control" id="amount" placeholder="0">
					</div>
					<select type="text" name="incomeFreq" id="incomeFreq" class="form-control" id="amount" placeholder="0">
						<option value="weekly">Weekly</option>
						<option value="fortnightly" selected="selected">Fortnightly</option>
						<option value="monthly">Monthly</option>
						<option value="annually">Annually</option>
					</select>
				</div>
				<br>
				<br>
			  	<label class="col-sm-3 control-label"></label>
				<button type="submit" value="submit" name="budgetSubmit" class="btn btn-primary">Submit</button>
			</form>
		
			<?php
			}else{
				$budget = json_decode(file_get_contents("users/".$_SESSION['user']."/budget.json"), true);
				$t_food = 0; $t_transport = 0; $t_entertainment = 0; $t_clothing = 0; $t_other = 0; $t_bills = 0; $t_total = 0;
				
				if(file_exists("users/".$_SESSION['user']."/expenses.json")){
					$all_expenses = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
					unset($months_expense);
					foreach($all_expenses as $v){
						if ( $v['month'] != date("n") ) {
							continue;
						}
						$months_expense[] = $v;
					}
					foreach($months_expense as $v){
						if($v["type"] == "Food"){
							$t_food += $v["amount"];
						}else if($v["type"] == "Transport"){
							$t_transport += $v["amount"];
						}else if($v["type"] == "Entertainment"){
							$t_entertainment += $v["amount"];
						}else if($v["type"] == "Clothing"){
							$t_clothing += $v["amount"];
						}else if($v["type"] == "Other"){
							$t_other += $v["amount"];
						}else if($v["type"] == "Bills"){
							$t_bills += $v["amount"];
						}
					}
					$t_total = $t_food + $t_transport + $t_entertainment + $t_clothing + $t_other + $t_bills;
				}else{
					
				}
			?>
			<div style="padding: 20px">
			<?php 
				$monthly_income = 0;
				if($budget["incomeFreq"] == "weekly"){
					$monthly_income = $budget["incomeAmount"] * 52/12;
				}else if($budget["incomeFreq"] == "fortnightly"){
					$monthly_income = $budget["incomeAmount"] * 52/2/12;
				}else if($budget["incomeFreq"] == "monthly"){
					$monthly_income = $budget["incomeAmount"];
				}else if($budget["incomeFreq"] == "annually"){
					$monthly_income = $budget["incomeAmount"] / 12;
				}
				$monthly_budget = 0;
				if($budget["budgetFreq"] == "weekly"){
					$monthly_budget = $budget["budgetAmount"] * 52/12;
				}else if($budget["budgetFreq"] == "fortnightly"){
					$monthly_budget = $budget["budgetAmount"] * 52/2/12;
				}else if($budget["budgetFreq"] == "monthly"){
					$monthly_budget = $budget["budgetAmount"];
				}
				
				if($t_total > $monthly_income){
					$temp = round($t_total-$monthly_income, 2);
					echo "<div class=\"alert alert-danger fade in\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a><strong>Warning:</strong> You have spent <b>$".money_format("%.2n",$temp)."</b> over your <b>monthly income</b>, please reduce your spending!</div>";
				}else if($t_total > $monthly_budget){
					$temp = round($t_total-$monthly_budget, 2);
					echo "<div class=\"alert alert-warning fade in\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a><strong>Warning:</strong> You have spent <b>$".money_format("%.2n",$temp)."</b> over your <b>monthly budget</b>, please reduce your spending.</div>";
				}
				
			?>
				<legend>Summary for <b><?php echo date("F")." ".date("Y") ?></b></legend><br>
				<div class="well">
				
					<?php
						if($t_total > $monthly_income){
						        echo "<h5>You have spent <b>$".money_format("%.2n",$t_total)."</b> this month</h5>";
						}else if($t_total > $monthly_budget){
						        echo "<h5>You have spent <b>$".money_format("%.2n",$t_total)."</b> this month</h5>";
						}else{
							$temp = round($monthly_budget-$t_total, 2);
							echo "<h5>You have spent <b>$".money_format("%.2n",$t_total)."</b> this month, you can still spend <b>$".money_format("%.2n",$temp)."</b> more this month</h5>";
						}
						echo "<h5>Your ".$budget['incomeFreq']." income is <b>$".$budget['incomeAmount']."</b></h5>";
						echo "<h5>Your ".$budget['budgetFreq']." budget is <b>$".$budget['budgetAmount']."</b></h5>"; 
					?>
					<br>
					<form action="editDetails.php" method="POST">
					<button class="btn btn-primary" type="submit"  name="editDetails">Edit your details</button>
					</form>
				</div>
		
			<?php 
			if(file_exists("users/".$_SESSION['user']."/expenses.json") && 
					file_exists("users/".$_SESSION['user']."/budget.json") ){
				//Prepare data for pie chart
				
				/* CAT:Pie charts */ 
				/* pChart library inclusions */ 
				include("pChart/class/pData.class.php"); 
				include("pChart/class/pDraw.class.php"); 
				include("pChart/class/pPie.class.php"); 
				include("pChart/class/pImage.class.php"); 

				/* Create and populate the pData object */ 
				$MyData = new pData(); 
				//array data points
				$MyData->addPoints(array($t_food,$t_transport,$t_entertainment,$t_clothing,$t_other,$t_bills),"ScoreA");   
				$MyData->setSerieDescription("ScoreA","Application A"); 

				/* Define the absissa serie */ 
				$MyData->addPoints(array("A","B","C"),"Labels"); 
				$MyData->setAbscissa("Labels"); 

				/* Create the pChart object */ 
				$myPicture = new pImage(400,200,$MyData,TRUE); 
				 
				/* Set the default font properties */  
				$myPicture->setFontProperties(array("FontName"=>"pChart/fonts/Forgotte.ttf","FontSize"=>20,"R"=>0,"G"=>0,"B"=>0)); 

				/* Create the pPie object */  
				$PieChart = new pPie($myPicture,$MyData); 

				/* Define the slice color */ 
				$PieChart->setSliceColor(0,array("R"=>50,"G"=>200,"B"=>50)); //food
				$PieChart->setSliceColor(1,array("R"=>50,"G"=>50,"B"=>200)); //transport
				$PieChart->setSliceColor(2,array("R"=>50,"G"=>200,"B"=>200)); //entertainment
				$PieChart->setSliceColor(3,array("R"=>200,"G"=>50,"B"=>200)); //clothing
				$PieChart->setSliceColor(4,array("R"=>150,"G"=>150,"B"=>150)); //other
				$PieChart->setSliceColor(5,array("R"=>200,"G"=>50,"B"=>50)); //bills

				/* Enable shadow computing */  
				$myPicture->setShadow(TRUE,array("X"=>3,"Y"=>3,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10)); 

				/* Draw a splitted pie chart */  
				$PieChart->draw3DPie(200,110,array("Radius"=>160,"WriteValues"=>TRUE,"DataGapAngle"=>0,"DataGapRadius"=>0,"Border"=>TRUE)); 

				/* Render the picture (choose the best way) */ 
				$myPicture->render("cache/".$_SESSION["user"]."-draw3DPie.png");
			}
			?>

			<?php 
			if(file_exists("cache/".$_SESSION["user"]."-draw3DPie.png")){
			?>
				<br>
				<legend>This months expenses by category</legend>
				<center>
				<div class="row">
					<div class="col-md-6">
						<br><br>
						<img src="cache/<?php echo $_SESSION["user"]; ?>-draw3DPie.png" alt="Chart" style="max-width:100%;max-height:100%;">
					</div>
					<div class="col-md-6">
						<table class="table table-striped" style="width: 100%;">
							<thead>
								<tr>
									<th>Colour</th>
									<th>Category</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								<?php if($t_bills != 0){ ?>
								<tr>
									<td BGCOLOR="#C83232"></td>
									<td>Bills</td>
			                                                <td>$<?php echo money_format("%.2n",$t_bills) ?></td>
								</tr>
								<?php }if($t_food != 0){ ?>
								<tr>
									<td BGCOLOR="#32C832"></td>
									<td>Food</td>
			                                                <td>$<?php echo money_format("%.2n",$t_food) ?></td>
								</tr>
								<?php }if($t_transport != 0){ ?>
								<tr>
									<td BGCOLOR="#3232C8"></td>
									<td>Transport</td>
			                                                <td>$<?php echo money_format("%.2n",$t_transport) ?></td>
								</tr>
								<?php }if($t_entertainment != 0){ ?>
								<tr>
									<td BGCOLOR="#32C8C8"></td>
									<td>Entertainment</td>
			                                                <td>$<?php echo money_format("%.2n",$t_entertainment) ?></td>
								</tr>
								<?php }if($t_clothing != 0){ ?>
								<tr>
									<td BGCOLOR="#C832C8"></td>
									<td>Clothing</td>
			                                                <td>$<?php echo money_format("%.2n",$t_clothing) ?></td>
								</tr>
								<?php }if($t_other != 0){ ?>
								<tr>
									<td BGCOLOR="#969696"></td>
									<td>Other</td>
			                                                <td>$<?php echo money_format("%.2n",$t_other) ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					</center>
				</div>
			<?php
			}
			?>
		<?php
		}
		?>
		</div>
	</div>
</div>
	
</body>

</html>
