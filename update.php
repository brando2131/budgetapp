<?php include("start.php"); custom_start();
        if($_SESSION['loginVerified'] != "success") {
	        header('Location: login.php');
		exit;
	}
        
        $user = $_SESSION['user'];
        if(is_dir("users/$user")) {
	  $data = json_decode(file_get_contents("users/$user/details.json"),true);
	  $email = $data['email'];
	}

?>

<html lang="en">
<head>
<?php include 'headerInfo.php' ?>
</head>
<body>
<?php include 'navbar.php' ?>
   <div class="container">
     <div class="jumbotron">

       <div class="block-center">
         <form method="POST" action="account.php">
           <h3 class="block-center-heading">Edit Profile</h3>
           <div class="form-group">
             <label for="user"><b>Account Name</b></label>
               <input class="form-control" style="width:300px;" id="user" name="user" maxlength="254" type="username" value=<?php echo "\"".$user."\""; ?> autofocus required/>
           </div>

	   <div class="form-group">
             <label for="email"><b>Email</b></label>
               <input class="form-control" style="width:300px;" id="email" name="email" maxlength="254" type="email" value=<?php echo "\"".$email."\""; ?> required/>
           </div>

	   <div class="form-group">
	       <button type="submit" class="btn btn-primary">Update</button>
	       <button href="account.php" class="btn btn-default">Cancel</button>
           </div>
         </form>
       </div>

     </div>
   </div>
</body>
</html>
