<?php
	function custom_start(){
		//lifetime/session cookie lasts for at least 2 hours
		ini_set('session.gc_maxlifetime', 2*3600);
		session_set_cookie_params(2*3600);
		session_start();
	}
?>