<?php include("start.php"); custom_start();
	//If not verified, do not continue, redirect back to login.php
	if($_SESSION["loginVerified"] != "success"){
		header('Location: login.php');
		exit;
	}
	
	//creating file of expenses
	////////If form has been posted to itself, attempt to add an expense
	////////Otherwise this is ignored
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$day = $_POST['day'];
		$month = $_POST['month'];
		$year = $_POST['year'];
		$amount = round($_POST['amount'], 2);
		$type = $_POST['type'];
		$description = $_POST['description'];
		$user = $_SESSION['user'];
		
		//Check if entries are valid
		$status = 'error';
		
		//Check if day is valid
		if(($month == 2 || $month ==  4 || $month == 6 || $month == 9 || $month == 11) && $day > 30 ){
			$message = 'Please enter a valid date';
			
		} else if ($month == 2 && $day > 28) {//not counting leap years
				$message = 'Please enter a valid date';
		
		//Check if amount is positive
		} else if(!is_numeric($amount) || $amount <= 0) {
				$message = 'Please enter an amount greater than $0';
				
		//success
		} else {
			//create/add to expenses file
			$id = 0;
			if(file_exists("users/".$_SESSION['user']."/expenses.json")){
				$U_EXPENSE = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
				$EXPENSE_ARRAY = $U_EXPENSE;
				$id = 1;
			}
			
			$USER_EXPENSE["day"] = $day;
			$USER_EXPENSE["month"] = $month;
			$USER_EXPENSE["year"] = $year;
			$USER_EXPENSE["amount"] = $amount;
			$USER_EXPENSE["type"] = $type;
			$USER_EXPENSE["description"] = $description;
			$USER_EXPENSE["id"] = $id;
			if ($id == 1) {
				foreach ($EXPENSE_ARRAY as $i) {
					if ($id <= $i['id']) $id = $i['id']+1;
				}
			}
			$USER_EXPENSE["id"] = $id;
			$EXPENSE_ARRAY[] = $USER_EXPENSE;
			foreach ($EXPENSE_ARRAY as $k) {
				$days[] = $k['day'];
			}
			array_multisort($days, SORT_NUMERIC, SORT_DESC, $EXPENSE_ARRAY);
			foreach ($EXPENSE_ARRAY as $j) {
				$months[] = $j['month'];
			}
			array_multisort($months, SORT_NUMERIC, SORT_DESC, $EXPENSE_ARRAY, SORT_DESC);
			file_put_contents("users/$user/expenses.json", json_encode($EXPENSE_ARRAY));
			$message = 'Expense has been added. Add another one or ';
			$status = 'success';
		}
	} else {
		echo $message = '';
	}
	
	if($message == ''){ //No alert box
		$alert = '';
	}else if($status == 'success'){ //Green alert box
		$alert = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong> <a href="index.php">go back to the homepage</a></div>';
		$_SESSION['alert'] = $alert;
	}else{ //Red alert box
		$alert = '<div class="alert alert-warning fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'.$message.'</strong></div>';
		$_SESSION['alert'] = $alert;
	}
	
	if(!isset($_GET['month'])) {
		$curMonth = date("n")+1;
		header("Location: addExpense.php?month=$curMonth");
		exit;
	}
	
	//deleting expense
	if (isset($_GET['delete']) && file_exists("users/".$_SESSION['user']."/expenses.json")) {
		$EXP_DEL = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
		$counter = 0;
		foreach ($EXP_DEL as $del) {
			if ($del['id'] == $_GET['delete']) {
				unset($EXP_DEL[$counter]);
				$EXP_DEL = array_values($EXP_DEL);
			}
			$counter++;
		}
		file_put_contents("users/".$_SESSION['user']."/expenses.json", json_encode($EXP_DEL));
		$curMonth = $_GET['month'];
	}

?>

<html lang="en">
<head>
	<?php include 'headerInfo.php' ?> 
</head>
<body>

<?php include 'navbar.php' ?>

	<div class="container">
		<div class='jumbotron'>
			<legend>Add expense</legend>
			<p>
			<div class="well">
			<?php //Display the alert box
				if (isset($_SESSION['alert'])) $alert = $_SESSION['alert'];
				echo $alert;
				$_SESSION['alert'] = "";
				file_put_contents("users/".$_SESSION['user']."/alert.json", json_encode($alert));
			?>
			<?php //Get current date
				$curDay = date("j");
				$curMonth = date("F");
				$curYear = date("Y");
			?>
		  		<form class="form-inline" action="addExpense.php" method="POST">
		  			<label for="date" class="col-sm-2 control-label">Date</label>
					<div>
						<select class="form-control" name="day">
							<?php //Print 31 days as options and auto-select current day
								for($i = 1; $i <= 31; $i++){
									if($curDay == $i){
										echo "<option value=".$i." selected=\"selected\">".$i."</option>";
									}else{
										echo "<option value=".$i.">".$i."</option>";
									}
								}
							?>
						</select> 
						<select class="form-control" name="month">
							<?php //Print month options and auto-select current month
								$months = array('January','February','March','April','May','June','July','August','September','October','November','December');
								$i = 1;
								foreach($months as $month){
									if($curMonth == $month){
										echo "<option value=".$i." selected=\"selected\">".$month."</option>";
									}else{
										echo "<option value=".$i.">".$month."</option>";
									}
									$i++;
								}
							?>
						</select> 
						<select class="form-control" name="year">
							<option value=2015 selected="selected">2015</option>
						</select> 
					</div>
		  			<p>
		  			
		  			<div>
			  			<label for="amount" class="col-sm-2 control-label">Amount</label>
						<label class="sr-only" for="amount"></label>
						<div class="input-group">
						  	<div class="input-group-addon">$</div>
					 	 	<input type="text" name="amount" class="form-control" id="amount" placeholder="0">
						</div>
					</div>
					<p>
					
					<div>
						<label for="type" class="col-sm-2 control-label">Category</label>
						<select class="form-control" name="type">
							<option value="Bills">Bills</option>
							<option value="Food">Food</option>
							<option value="Transport">Transport</option>
							<option value="Entertainment">Entertainment</option>
							<option value="Clothing">Clothing</option>
							<option value="Other" selected="selected">Other</option>
						</select> 
					</div>
					<p>
					
					<div>
						<label for="description" class="col-sm-2 control-label">Description</label>
						<input type="text" name="description" class="form-control" id="desciption">
					</div>
					<br>
			  	
			  		<label class="col-sm-2 control-label"></label>
			  		<button type="submit" class="btn btn-primary">Submit</button>
			  	
				</form>
			</div>
			<br>
			<legend>This month's expenses</legend>
			
			<form class="form-inline">
				<div class="form-group">
					<?php
					for ($count = 2; $count < 14; $count++) {
						if (isset($_GET['month'])) {
							if ($count == $_GET['month']) {
								echo "<a class=\"btn disabled\" href=\"addExpense.php?month=$count\" type=\"submit\" role=\"button\" name=\"month\" value=\"$count\"><h3>";
								echo date("M", mktime(0, 0, 0, $count, 0, 2015));
								echo "</h3></a>\n";
							} else {
								echo "<a class=\"btn\" href=\"addExpense.php?month=$count\" type=\"submit\" role=\"button\" name=\"month\" value=\"$count\">";
								echo date("M", mktime(0, 0, 0, $count, 0, 2015));
								echo "</a>\n";
							}
						} else if ($count-1 == date("n")) {
							echo "<a class=\"btn disabled\" href=\"addExpense.php?month=$count\" type=\"submit\" role=\"button\" name=\"month\" value=\"$count\"><h3>";
							echo date("M", mktime(0, 0, 0, $count, 0, 2015));
							echo "</h3></a>\n";
						} else {
							echo "<a class=\"btn\" href=\"addExpense.php?month=$count\" type=\"submit\" role=\"button\" name=\"month\" value=\"$count\">";
							echo date("M", mktime(0, 0, 0, $count, 0, 2015));
							echo "</a>\n";
						}
					}
					?>
				</div>
			</form>
			
			<table class="table table-striped" style="width: 100%;">
				<thead>
					<tr>
						<th>Date</th>
						<th>Amount</th>
						<th>Category</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(file_exists("users/".$_SESSION['user']."/expenses.json")){
						$U_EXPENSE_DECODE = json_decode(file_get_contents("users/".$_SESSION['user']."/expenses.json"), true);
						
						echo "\n";
						//Print each entity for table
						
						foreach($U_EXPENSE_DECODE as $v){
							if (isset($_GET['month'])) {
								if ($v['month'] != $_GET['month']-1) continue;
							} else {
								if ($v['month'] != date("n")) continue;
							}
							echo "<tr>\n";
							
							if ($v['day'] < 10 && $v['month'] < 10) {
								echo "<td>".'0'.$v['day'].'/0'.$v['month'].'/'.$v['year']."</td>\n";
							} else if ($v['day'] < 10 && $v['month'] > 9) {
								echo "<td>".'0'.$v['day'].'/'.$v['month'].'/'.$v['year']."</td>\n";
							} else if ($v['day'] > 9 && $v['month'] < 10) {
								echo "<td>".$v['day'].'/0'.$v['month'].'/'.$v['year']."</td>\n";
							} else {
								echo "<td>".$v['day'].'/'.$v['month'].'/'.$v['year']."</td>\n";
							}
							echo "<td>".'$'.$v['amount']."</td>\n";
							echo "<td>".$v['type']."</td>\n";
							echo "<td>".$v['description']."</td>\n";
							echo "<td>\n<form action=\"addExpense.php\" method=\"get\">\n<input type=\"hidden\" value=\"".$_GET['month']."\" name=\"month\">\n";
							echo "<input type=\"hidden\" value=\"".$v['id']."\" name=\"delete\">\n";
							echo "<input type=\"submit\" class=\"btn btn-danger btn-xs\" value=\"X\"></form></td>\n";
							echo "</tr>\n";
							$i++;
						}
					}
				?>
				</tbody>
			</table>
			
			
			
			<a class="btn btn-default" href="viewExpense.php" role="button">View all</a>
			
		</div>
	</div>
</body>

</html>
